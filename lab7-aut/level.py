#levelpy tu radim sve kolizije 
import pygame
import random
from entities import Player, Asteroid, Turret, Explosion, LaserHit, HealthPack
from helpers import render_text

class Level(object):
    LEVEL_START_TIMEOUT = 60
    HP_START = 0
    
    CONFIG = {
        1: {"asteroid_count": 5 },
        2: {"asteroid_count": 8 },
        3: {"asteroid_count": 10},
        4: {"asteroid_count": 13},
        5: {"asteroid_count": 13},
        6: {"asteroid_count": 15},
        7: {"asteroid_count": 20},
        8: {"asteroid_count": 25},
    }
    SHIP_ON = pygame.image.load("ship_on.png")
    SHIP_OFF = pygame.image.load("ship_off.png")
    ASTEROID = pygame.image.load("asteroid.png")
    ASTEROID_MASK = pygame.image.load("asteroid_mask.png")
    LASER = pygame.image.load("laser.png")
    TURRET = pygame.image.load("turret.png")
    TURRET_LASER = pygame.image.load("turret_laser.png")
    EXPLOSION_SHEET = pygame.image.load("explosion.png")
    WRENCH = pygame.image.load("wrench.png")

    def __init__(self, screen, level):
        self.frame = 0
        self.screen = screen
        self.sw, self.sh = self.screen.get_size()
        self.state = "play" # "play", "won", "lost"
        self.score = 0

        self.player = Player(self)
        #self.turret = Turret(self)

        self.healthpack = HealthPack(self)
        self.turrets = []
        for i in range(2):
            #self.turrets.append(Turret(level))
            self.turrets.append(Turret (self) )

        self.ASTEROID_COUNT = self.CONFIG[level]["asteroid_count"]
        self.asteroids = []
        for i in range(self.ASTEROID_COUNT):
               self.asteroids.append( Asteroid(self))

        self.bullets = []
        self.turret_bullets = []
        self.explosions = [] #prazna lista

        self.healthpacks = []
        for i in range(1):
            self.healthpacks.append(HealthPack(self))
        
    def update(self):
        self.frame += 1
        for asteroid in self.asteroids:
            asteroid.update()
        self.player.update()
        for bullet in self.bullets:
            bullet.update()
        for turret in self.turrets:
            turret.update()
        for bullet in self.turret_bullets:
            bullet.update()
        for e in self.explosions:
            e.update()
        #self.healthpack.update()
            for healthpack in self.healthpacks:
                healthpack.update()

            if self.HP_START == 1:
                self.HP_START = 0
                i = 0
                for i in range(6000):
                    if i >= 5999:
                        print(i)
                        for healthpack in range(1):
                            self.healthpacks.append(HealthPack(self))
                        i = 0

        self.check_collisions()
        self.clear_dead_entities()

        for asteroid in self.asteroids:
            asteroid.draw()
        for bullet in self.bullets:
            bullet.draw()
        for bullet in self.turret_bullets:
            bullet.draw()
        self.player.draw()
        for turret in self.turrets:
            turret.draw()
        for e in self.explosions:
            e.draw()
        self.player.draw()

        #self.healthpack.draw()
        for healthpack in self.healthpacks:
            healthpack.draw()
            
        bullet_count = render_text("Bullets: %d"%len(self.bullets), 16)
        self.screen.blit(bullet_count, (20, self.sh-36))
        health = render_text("Health: %d"%self.player.health,20)
        self.screen.blit(health, (20,60))

        turrets_health = " | ".join([str (turret.health) for turret in self.turrets])
        turrets_health = "Turrets: %s "%turrets_health
        self.screen.blit(render_text(turrets_health,20) , (20,100))

        #Handle states
        
        if not self.player.alive:
            self.state = "lost"
        if len(self.asteroids) == 0:
            self.state = "won"

    def check_collisions(self):
        if self.frame < self.LEVEL_START_TIMEOUT:
            return False
        for asteroid in self.asteroids:
            for bullet in self.bullets:
                if bullet.collided_with(asteroid):
                    bullet.alive = False
                    asteroid.alive = False
                    self.score += 1
                    self.explosions.append(Explosion(asteroid))
            if self.player.collided_with(asteroid):
                asteroid.alive = False
                self.player.alive = False
        for bullet in self.turret_bullets:
             if bullet.collided_with(self.player):
                self.explosions.append(LaserHit(bullet))
                self.player.hit()  #na ovaj nacin kazemo playeru da je pogoden, i ako je sam si oduzme health, ubije se
                bullet.alive=False #nakon toga odes u playera u entities i napravis hit tamo
        for bullet in self.bullets:
            for turret in self.turrets:
                if bullet.collided_with(turret):
                    self.explosions.append(LaserHit(bullet))
                    bullet.alive= False
                    turret.hit()
                if turret.alive == False:
                    self.explosions.append(Explosion(turret))
                    
      # for turret in self.turrets:
       #     for player in self.player:
        #        if turret.collided and player.self.collided:
         #           player.alive = False
         ########################## prvi pokušaj kolizije između playera i turreta

        for turret in self.turrets:
            if self.player.collided_with(turret):
                self.player.alive=False

    


        for healthpack in self.healthpacks:
            if self.player.collided_with(healthpack):
                self.player.heal()
                healthpack.hit()
                if healthpack.alive == False:
                    print("game over")
                    self.HP_START=1
                    
        #if self.player.collided_with(self.healthpack):
        #    self.player.heal()
        #    self.healthpack.alive = False
            #del self.healthpack
                          
    def clear_dead_entities(self):
        for i, bullet in enumerate(self.bullets):
            if not bullet.alive:
                del self.bullets[i]

        for i, asteroid in enumerate(self.asteroids):
            if not asteroid.alive:
                del self.asteroids[i]
        for i, bullet in enumerate(self.turret_bullets): #tu brises metak iz igre nakon sto pogodi igraèa
            if not bullet.alive:
                del self.turret_bullets[i]
            

        for i, turret in enumerate(self.turrets): # tu brisemo turret dakle ako ga dovoljno puta pogodimo nece vise bit alive
            if not turret.alive:
                del self.turrets[i]

        for i, explosion in enumerate(self.explosions):
            if not explosion.alive:
                del self.explosions[i]

            
        for i, healthpack in enumerate(self.healthpacks):
            if not healthpack.alive:
                del self.healthpacks[i]
