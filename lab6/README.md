# Lab 6 - My Lame Imgur

Lab tutorial:

1. start django project
2. save requirements.txt
2. start django app called `app`
3. register app to settings.py
4. make migrations, migrate, create super user
5. set up routing to app
5. create simple views to test routing
6. create models
7. make migrations and migrate
8. create index.html and detail.html templates
9. Create views which will render these templates
10. Add base.html template with main layout
11. add css and static assets
12. show comments on detail view
13. show number of comments on index
14. show upvotes/downvotes on index and on detail
15. Add comments to detail view
16. Add form for commenting for detail view


## Tasks

1. enable upvoting/downvoting of detail view
2. enable showing comment form only to logged in user
3. add form for new image submission to index view for logged in users

## LINKS

https://steelkiwi.com/blog/best-practices-working-django-models-python/

