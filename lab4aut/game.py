# ukljucivanje biblioteke pygame
import pygame
import random

class Bob(object):

    def __init__(self, screen):
        self.screen = screen
        self.screen_w, self.screen_h = screen.get_size()
        self.x = random.randint(0, self.screen_w)
        self.y = random.randint(0, self.screen_h)
        self.dx = 1
        self.dy = 1
        self.dirx = 1
        self.diry = 1
        self.size= [100,100]
        self.speed_factor = 1
        self.hit = False
        self.original_image = pygame.image.load("bob.png")
        self.image = None
        self.drawn = None
        self.angle = 0
        
    def draw(self):
        self.angle = (self.angle + 1) % 360 
        self.image = pygame.transform.scale(self.original_image, (self.size))
        self.image = pygame.transform.rotate(self.image, self.angle)
        self.drawn = screen.blit(self.image, (self.x,self.y))
    def move(self):
        self.dx = self.dirx * self.speed_factor
        self.dy = self.diry * self.speed_factor
        self.x += self.dx
        self.y += self.dy
        if self.x > self.screen_w - 100 or self.x <0:
            self.dirx = self.dirx * -1
        if self.y > self.screen_h - 100 or self.y <0:
            self.diry = self.diry * -1
    def move_to_random_position(self):
        self.x = random.randint(0, self.screen_w - 100)
        self.y = random.randint(0, self.screen_h - 100)
    def is_hit(self, pos):
        if self.drawn.collidepoint(pos):
            self.hit = True
            self.move_to_random_position()
            self.speed_factor +=1
            self.size[0] -= 10
            self.size[1] -= 10
            return True
        else:
            return False




def center(size, fig):
    figw = fig.get_rect()[2]
    figh = fig.get_rect()[3]
    w = size[0]
    h = size[1]
    return (w/2 - figw/2, h/2 - figh/2)
    
def load_fig(name, size):
    fig = pygame.image.load(name)
    fig = pygame.transform.scale(fig, size)
    return fig

god_mode = True  
pygame.init()
# definiranje konstanti za velicinu prozora
WIDTH = 1024
HEIGHT = 600
# tuple velicine prozora
size = (WIDTH, HEIGHT)
WHITE = (255,255,255)
BLACK = (0,0,0)
RED = (255,0,0)
GREEN = (0,255,0)

#definiranje novog ekrana za igru
screen = pygame.display.set_mode(size)

#definiranje naziva prozora
pygame.display.set_caption("Ćikim")

myfont = pygame.font.SysFont('Arial', 30)
welcome_text= myfont.render("Welcome to my game", False, BLACK)
end_text = myfont.render("GAME OVER", True, WHITE)

welcome_bg = load_fig("bg.jpg", size)
end_bg = load_fig("end.jpg", size)
bobs = []
for i in range(10):
    bobs.append( Bob(screen))


clock = pygame.time.Clock()

state = "welcome"
bg= RED
done = False
while not done:
    #event petlja
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if state == "game":
                pos = event.pos
                for bob in bobs[::-1]:
                   hit = bob.is_hit(pos)
                   if hit:
                        break
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                if state == "game":
                    if bg == GREEN:
                        bg = RED
                    elif bg == RED:
                        bg = BLACK
                    elif bg == BLACK:
                        bg = GREEN
                elif state == "end":
                    state = "welcome"
            elif event.key == pygame.K_RETURN:
                if state == "welcome":
                    state = "game"
            elif event.key == pygame.K_ESCAPE:
                if state == "game":
                    state = "end"
    #state processing
    if state == "game":
        for bob in bobs:
            bob.move()
            
        game_time -= clock.get_time()
        if game_time < 0 and not god_mode:
            state = "end"
        if hit:
            game_time_start *= 0.9
            game_time = game_time_start
            score += 1
            hit = False
    elif state == "welcome":
        game_time_start = 3000
        game_time = game_time_start
        score = 0
        hit = False


    #iscrtavanja
    if state == "welcome":
        screen.blit(welcome_bg, (0,0))
        screen.blit(welcome_text, center(size, welcome_text))
    elif state == "game":
        screen.fill(bg)
        for bob in bobs:
            bob.draw()
        time = myfont.render("Time: %d"%game_time, True, WHITE)
        screen.blit(time, (30, HEIGHT - 100))
        score_text = myfont.render("Score: %d"%score, True, WHITE)
        screen.blit(score_text, (30, 30))
    elif state == "end":
        screen.blit(end_bg, (0,0))
        screen.blit(end_text, center(size, end_text))    
    
    pygame.display.flip()

    #ukoliko je potrebno ceka do iscrtavanja 
    #iduceg framea kako bi imao 60fpsa
    clock.tick(60)

pygame.quit()
